﻿using System;

namespace BT_Cau9
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle1 = new Rectangle() { Width = 10, Height = 20 };
            Rectangle rectangle2 = new Rectangle() { Width = 5, Height = 9 };

            //Cong hai Rectangle
            Rectangle rectangleSum = rectangle1 + rectangle2;

            //tru hai Rectangle
            Rectangle rectangleSub = rectangle1 - rectangle2;
        }
    }
}
