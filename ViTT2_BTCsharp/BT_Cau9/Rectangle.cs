﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BT_Cau9
{
    public class Rectangle : Shape
    {
        public override void Area(int width, int height)
        {
            var dienTich = 0;
            dienTich = width * height;

        }

        public void Size(int size)
        {
            Console.WriteLine($"size: {size}");
        }

        public void Size(int width, int height)
        {
            Console.WriteLine($"Width: {width} \tHeight: {height}");
        }

        public static Rectangle operator +(Rectangle rectangle1, Rectangle rectangle2)
        {
            Rectangle sum = new Rectangle()
            {
                Width = rectangle1.Width + rectangle2.Width,
                Height = rectangle1.Height + rectangle2.Height
            };
            Console.WriteLine($"Width: {sum.Width} \tHeight: {sum.Height}");
            return sum;
        }
        
        public static Rectangle operator -(Rectangle rectangle1, Rectangle rectangle2)
        {
            Rectangle sub = new Rectangle()
            {
                Width = rectangle1.Width - rectangle2.Width,
                Height = rectangle1.Height - rectangle2.Height
            };
            Console.WriteLine($"Width: {sum.Width} \tHeight: {sum.Height}");
            return sub;
        }
    }
}
