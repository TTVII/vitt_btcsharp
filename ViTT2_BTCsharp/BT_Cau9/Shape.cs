﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BT_Cau9
{
    public class Shape
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public virtual void Area(int width, int height)
        {

        }
    }
}
