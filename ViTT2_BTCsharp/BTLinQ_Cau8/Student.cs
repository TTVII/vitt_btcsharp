﻿namespace BTLinQ_Cau8
{
    public class Student
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int ClassId { get; set; }
    }
}
