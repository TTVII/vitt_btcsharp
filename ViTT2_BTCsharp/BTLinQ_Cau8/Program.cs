﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BTLinQ_Cau8
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>()
            {
                new Student{Name = "vi", Age = 20, ClassId = 1},
                new Student{Name = "vuong", Age = 25, ClassId = 2},
                new Student{Name = "van", Age = 21, ClassId = 2},
                new Student{Name = "vang", Age = 15, ClassId = 1},
                new Student{Name = "va", Age = 30, ClassId = 1},
            };

            List<ClassSchool> classSchools = new List<ClassSchool>()
            {
                new ClassSchool{ClassId = 1, ClassName = "anh van"},
                new ClassSchool{ClassId = 2, ClassName = "toan"},
                new ClassSchool{ClassId = 3, ClassName = "vat li"},
                new ClassSchool{ClassId = 4, ClassName = "hoa"},
                new ClassSchool{ClassId = 5, ClassName = "ngu van"},
            };

            //lấy danh sách học sinh trong lớp
            var groupJoin = classSchools.GroupJoin(students,
                                                std => std.ClassId,
                                                cl => cl.ClassId,
                                                (std, studentgroup) => new
                                                {
                                                    students = studentgroup,
                                                    ClassName = std.ClassName
                                                });

            foreach(var item in groupJoin)
            {
                Console.WriteLine(item.ClassName);
                Console.WriteLine(item.students.Count());
                foreach (var stud in item.students)
                {
                    Console.WriteLine(stud.Name);
                }
                
            } 
            

        }
    }
}
