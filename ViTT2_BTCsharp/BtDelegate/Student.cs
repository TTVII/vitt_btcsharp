﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BtDelegate
{
   public class Student
    {
        public int StudentId { get; set; }
        public string Name {get; set; }
        public string Birth { get; set; }
        public string Gender { get; set; }
    }
}
