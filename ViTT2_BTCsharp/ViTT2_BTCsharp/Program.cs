﻿using BTDay4;
using System;

namespace ViTT2_BTCsharp
{
    class Program
    {
        public const int phatCoBan = 100000;
        static void Main(string[] args)
        {
            
            int luaChon;
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("1. Nhập thông tin employee");
            Console.WriteLine("2. xem thông tin employee");
            Console.WriteLine("3. Update thông tin user");

            var emp = new Employee();
            while (true)
            {
                Console.Write("Nhập lựa chọn: ");
                luaChon = Convert.ToInt32(Console.ReadLine());
                if (luaChon == 1)
                {
                    emp.InputEmployee();
                }
                else if (luaChon == 2)
                {
                    emp.GetEmployee();
                }
                else if (luaChon == 3)
                {
                    emp.UpdateEmployee();
                }
                else if (luaChon == 0)
                    break;

               
            }
            Console.WriteLine($"count: {Employee.Count_check}" );
            if (Employee.Count_check > 40)
            {
                int tienPhat = 0;
                tienPhat = phatCoBan * (Employee.Count_check / 40);
                Console.WriteLine($"Anh quá khả nghi, mời anh lên phòng nộp phạt: {tienPhat}");
            }

        }
    }
}
