﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTDay4
{
    public class Employee
    {
        private int _Id = 0;
        private string _Name;
        private float _Salary;
        private string _Role;
        public static int Count_check = 0;
        public int ID
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        
        public float Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }public string Role
        {
            get { return _Role; }
            set { _Role = value; }
        }

        public void InputEmployee()
        {
            try
            {
                Console.Write($"Id nhân viên:");
                _Id = Convert.ToInt32(Console.ReadLine());
                Console.Write("Nhập tên: ");
                _Name = Console.ReadLine();
                Console.Write("Nhập lương: ");
                _Salary = float.Parse(Console.ReadLine());
                Console.Write("Nhập role: ");
                _Role = Console.ReadLine();
            }catch(Exception e)
            {
                Console.WriteLine($"Lỗi: {e.Message}");
            }
        }

        public void GetEmployee()
        {
            Console.WriteLine($"Id nhân viên: {_Id}");
            Console.WriteLine($"Tên nhân viên: {_Name}");         
            Console.WriteLine($"Lương: {_Salary}");
            Console.WriteLine($"Role: {_Role}");
            Count_check++;
        }

        public void UpdateEmployee()
        {
            try
            {
                Console.Write($"Id nhân viên:");
                _Id = Convert.ToInt32(Console.ReadLine());
                Console.Write("Nhập tên: ");
                _Name = Console.ReadLine();
                Console.Write("Nhập lương: ");
                _Salary = float.Parse(Console.ReadLine());
                Console.Write("Nhập role: ");
                _Role = Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Lỗi: {e.Message}");
            }

            if (_Role !=  "admin")
            {
                Count_check += 40;
            }
            else
            {
                Count_check += 1;
            }  
        }
    }
}
