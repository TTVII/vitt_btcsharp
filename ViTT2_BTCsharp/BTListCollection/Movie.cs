﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace BTListCollection
{
    public class Movie : IComparable<Movie>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Director { get; set; }

        public int CompareTo(Movie movie)
        {
            if (Id > movie.Id)
                return 1;
            else if (Id == movie.Id)
                return 0;
            else
                return -1;
        }

        public static Movie FindMovie4(string name, List<Movie> movies)
        {
            foreach(Movie item in movies)
            {
                if (item.Name == name)
                    return item;
            }
            return null;
        }
    }

    public class PlayMovie
    {
        public void ShowMovie()
        {

        }
    }

}
