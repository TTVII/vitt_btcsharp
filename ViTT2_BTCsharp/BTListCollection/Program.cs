﻿using System;
using System.Collections.Generic;

namespace BTListCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            //Them ptu vào list
            List<Movie> movies = new List<Movie>()
            {
                new Movie{Id = 1, Name = "Spider man", Director = "Director1"},
                new Movie{Id = 2, Name = "Spider man1", Director = "Director2"},
                new Movie{Id = 3, Name = "Spider man2", Director = "Director3"},
                new Movie{Id = 4, Name = "Movie4", Director = "Director4"}
            };

            //Xóa phần tử 
            movies.RemoveAt(1);

            //chèn ptu với index = 2

            movies.Insert(2, new Movie { Id = 5, Name = "Spider man5", Director = "Director5" });

            //sắp xếp
            movies.Sort(); //sắp xếp tăng dần
            movies.Reverse();// tạo một list mới ngược với list movies từ movies
            //show danh sách
            foreach( var movie in movies)
            {
                Console.WriteLine($"Id: {movie.Id} \tName: {movie.Name} \tDirector: {movie.Director}");
            }

            //tìm phần tử có giá trị name = Movie4
            var movie4 = Movie.FindMovie4("Movie4", movies);
        }
    }
}
