﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace BTListAndDictionary_Cau5
{
    public class Student : IComparable<Student>
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Classes { get; set; }

        public static int CheckStudent(int studentId, List<Student> students)
        {
            foreach(Student item in students)
            {
                if (item.StudentId == studentId)
                    return 1;
            }
            return 0;
        }

        public  int CompareTo( Student other)
        {
            if (StudentId > other.StudentId)
                return 1;
            else if (StudentId < other.StudentId)
                return -1;
            else
                return 0;

        }

        public static void ShowStudent(List<Student> students)
        {
            foreach (Student item in students)
            {
                Console.WriteLine($"StudentId: {item.StudentId} \tName: {item.Name} \tAge: {item.Age} \tClasses: {item.Classes}");
            }
        }

    }
}
