﻿using System;
using System.Collections.Generic;

namespace BTListAndDictionary_Cau5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            List<Student> students = new List<Student>()
            {
                new Student{StudentId = 1, Name = "Vi1", Age = 20, Classes = "A"},
                new Student{StudentId = 4, Name = "Vi2", Age = 21, Classes = "B"},
                new Student{StudentId = 2, Name = "Vi3", Age = 23, Classes = "C"},
                new Student{StudentId = 3, Name = "Vi4", Age = 24, Classes = "D"}
            };

            //kiêm tra 1 student
            Console.Write("Nhập id student muốn kt: ");
            int id = Convert.ToInt32(Console.ReadLine());
            var check = Student.CheckStudent(id, students);
            if(check == 1 )
                Console.WriteLine($"Student với id la {id} có tồn tại trong danh sách");
            else
                Console.WriteLine($"Student với id la {id} không tồn tại trong danh sách");
            

            //xắp xếp
            Console.WriteLine("List chưa xắp xếp");
            Student.ShowStudent(students);
            Console.WriteLine("List sau khi xắp xếp");
            students.Sort();
            students.Reverse();
            Student.ShowStudent(students);

            //xóa 1 student bất ki
            Console.Write("Nhập id student muốn xóa: ");
            int studentId = Convert.ToInt32(Console.ReadLine());
            int kt = Student.CheckStudent(studentId, students);
            if(kt == 1)
            {
                students.RemoveAt(studentId);
                Student.ShowStudent(students);
                Console.WriteLine($"Số phần tử còn lại trong List: {students.Count}");
            }else
                Console.WriteLine("StudentId không hợp lệ, không xóa dc.");
            Console.ReadKey();

        }
    }
}
