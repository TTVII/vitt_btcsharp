﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTIEnumerable
{
    public class Module
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EmployId { get; set; }
        public DateTime Date { get; set; }
        public DateTime? CompleteDate { get; set; }
        public int Status { get; set; }
    }
}
