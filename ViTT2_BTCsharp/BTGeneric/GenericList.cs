﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTGeneric
{
    public class GenericList<T>
    {
        private List<T> genericList = null;


        public GenericList()
        {
            genericList = new List<T>();
        }

        public void AddList(T t)
        {
            genericList.Add(t);
        }

        public void RemoveList(T t)
        {
            int index = genericList.IndexOf(t);
            genericList.Remove(genericList[index]);
        }

        public void ShowGennericList()
        {
            foreach(var item in genericList)
            {
                Console.WriteLine(item);
            }    
        }
    }
}
