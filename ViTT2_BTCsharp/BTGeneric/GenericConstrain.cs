﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BTGeneric
{
    public class GenericConstrain<T> where T : class 
    {
        public GenericConstrain()
        {
            
        }
        public void ShowProperty(T t)
        {
            Console.WriteLine($"Type of object: {t.GetType()}" );
            Console.WriteLine($"Value of object: {t}");
        }
    }
}
