﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BTGeneric
{
    class Program
    {
        static void Main(string[] args)
        {
            GenericList<string> genericList = new GenericList<string>();
            //add list
            genericList.AddList("dddd");
            genericList.AddList("bbbb");
            genericList.AddList("cccc");
            genericList.AddList("aaaa");
            genericList.ShowGennericList();

            //xoa
            Console.WriteLine("list sau khi xoa");
            genericList.RemoveList("dddd");
            genericList.ShowGennericList();

           
            Console.ReadLine();
        }
    }
}
