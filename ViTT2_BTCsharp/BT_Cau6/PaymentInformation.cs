﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BT_Cau6
{
    public abstract class PaymentInformation
    {
        public Guid _paymentNo { get; set; }
        public virtual Guid GetPaymentNo()
        {
            return _paymentNo;
        }

        public PaymentInformation()
        {
            _paymentNo = Guid.NewGuid();
        }

        public PaymentInformation(Guid paymentNo)
        {
            _paymentNo = paymentNo;
        }

    }
}
