﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BT_Cau6
{
    public interface IOrder
    {
        void PlaceOrder();
        void CancelOrder();
        void GetOrder();
    }
}
