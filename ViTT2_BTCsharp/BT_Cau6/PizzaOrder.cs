﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BT_Cau6
{
    public class PizzaOrder : PaymentInformation, IOrder
    {
        private int _price = 5000;
        private int _amount = 0;
        private bool _isOrder = false;

        public PizzaOrder()
        {

        }
        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public void CancelOrder()
        {
            if (_isOrder == true)
            {
                Console.WriteLine("Huy thanh cong.");
                _isOrder = false;
            }
            else Console.WriteLine("Chua dat hang.");

        }

        public void GetOrder()
        {
            Console.WriteLine($"Payment No: {GetPaymentNo()}");
            Console.WriteLine($"TOTAL PRICE: {_price * _amount}");
            Console.WriteLine($"STATUS: {_isOrder}\n");
        }

            public void PlaceOrder()
        {
            if(_amount == 0)
            {
                Console.WriteLine("Nhập amount: ");
                int am = Convert.ToInt32(Console.ReadLine());
                _amount = am;
            }else
            {
                if(_isOrder == true)
                {
                    Console.WriteLine("Bạn đã đặt rồi");
                }
                else
                {
                    Console.WriteLine("Order thành công");
                    _isOrder = true;
                    GetOrder();
                }
            }    
        }
    }
}
